package org.example.app.method.resolver;

import lombok.RequiredArgsConstructor;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.Pathvariable;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.exception.UnsupportedParameterException;
import org.example.framework.server.exception.UnsupportedParameterTypeException;
import org.example.framework.server.http.Request;

import java.lang.reflect.Parameter;
@Component
@RequiredArgsConstructor
public class PathVariableArgumentResolver implements ArgumentResolver {
    @Override

    public boolean supportParameter(final Parameter parameter) {
        return parameter.isAnnotationPresent(Pathvariable.class);
    }

    @Override
    public Object resolveArgument(final Parameter parameter, final Request request) throws Exception {
        if (!supportParameter(parameter)) {
            throw new UnsupportedParameterException(parameter.getName());
        }
        final Class<?> paramClazz = parameter.getType();
        final Pathvariable annotation = parameter.getAnnotation(Pathvariable.class);

        final String value = request.getPathGroup(annotation.value());
        if (paramClazz.equals(long.class)) {
            return Long.parseLong(value);
        }
        throw new UnsupportedParameterTypeException(parameter.getName());
    }
}
