package org.example.app.method.converter;


import lombok.RequiredArgsConstructor;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.controller.method.converter.HttpMessageConverter;
import org.example.framework.server.http.HttpStatus;
import org.example.framework.server.http.MediaType;
import org.example.framework.server.http.Request;
import org.example.framework.server.http.Response;

@Component
@RequiredArgsConstructor
public class PngHttpMessageConverter implements HttpMessageConverter {


    @Override
    public boolean canRead(final Class<?> clazz, final MediaType mediaType) {
        return MediaType.IMAGE_PNG.equals(mediaType);
    }

    @Override
    public boolean canWrite(final Class<?> clazz, final MediaType mediaType) {
        return MediaType.ANY.equals(mediaType);
    }

    @Override
    public Object read(final Class<?> clazz, final Request request) {
        return request.getBody();
    }

    @Override
    public void write(final Object object, final MediaType mediaType, final Response response) {
        if (object instanceof byte[]) {
            byte[] bytes = (byte[]) object;
            response.writeResponse(HttpStatus.S200, MediaType.IMAGE_PNG, bytes);
        }
    }
}
