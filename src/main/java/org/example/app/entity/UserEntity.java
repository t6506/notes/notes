package org.example.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserEntity {
  @Id
  @Column(nullable = false)
  private String login;
  @Column(nullable = false)
  private String encodePassword;
}
