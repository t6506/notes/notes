package org.example.app.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "notes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class NoteEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @ManyToOne
  @JoinColumn(updatable = false)
  private UserEntity owner;
  @Column(columnDefinition = "TEXT")
  private String content;
  @Column(nullable = false , columnDefinition = "boolean default false")
  private boolean removed;
  @Lob
  private byte[] image;
}
