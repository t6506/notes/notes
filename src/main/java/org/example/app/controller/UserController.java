package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.exception.LoginAlreadyRegisteredException;
import org.example.app.manager.UserManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.annotation.RequestBody;
import org.example.framework.server.annotation.RequestMapping;
import org.example.framework.server.annotation.ResponseBody;
import org.example.framework.server.http.HttpMethod;

@Controller
@Component
@RequiredArgsConstructor
public class UserController {
    private final UserManager manager;

    @RequestMapping(method = HttpMethod.POST, path = "/users$")
    @ResponseBody
    public UserRegisterRS register(@RequestBody final UserRegisterRQ registerRQ) throws LoginAlreadyRegisteredException {
        return manager.create(registerRQ);
    }


}
