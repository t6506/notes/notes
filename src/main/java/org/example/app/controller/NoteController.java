package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.LoadImageRS;
import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;
import org.example.app.manager.NoteManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.*;
import org.example.framework.server.http.HttpMethod;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Component
public class NoteController {
    private final NoteManager manager;

    @RequestMapping(method = HttpMethod.POST, path = "^/notes$")
    @ResponseBody
    public NoteRS create(@RequestBody final NoteRQ createRQ) {
        return manager.create(createRQ);
    }

    @RequestMapping(method = HttpMethod.GET, path = "^/notes/(?<noteId>[0-9]{1,9})$")
    @ResponseBody
    public NoteRS getById(@Pathvariable("noteId") final long id) {
        return manager.getById(id);
    }

    @RequestMapping(method = HttpMethod.GET, path = "^/notes$")
    @ResponseBody
    public List<NoteRS> getAll() {
        return manager.getAll();
    }

    @RequestMapping(method = HttpMethod.POST, path = "^/notes/(?<noteId>[0-9]{1,9})/update$")
    @ResponseBody
    public NoteRS update(@Pathvariable("noteId") final long id, @RequestBody final NoteRQ updateRQ) {
        return manager.updateContent(id, updateRQ);
    }

    @RequestMapping(method = HttpMethod.POST, path = "^/notes/(?<noteId>[0-9]{1,9})/remove$")
    @ResponseBody
    public void remove(@Pathvariable("noteId") final long id) {
        manager.removeById(id);
    }

    @RequestMapping(method = HttpMethod.POST, path = "^/notes/(?<noteId>[0-9]{1,9})/image$")
    @ResponseBody
    public LoadImageRS loadImage(@Pathvariable("noteId") final long id, @RequestBody final byte[] image) {
        return manager.loadImage(id, image);
    }

    @RequestMapping(method = HttpMethod.GET, path = "^/notes/(?<noteId>[0-9]{1,9})/image$")
    @ResponseBody
    public byte[] getImage(@Pathvariable("noteId") final long id) {
        return manager.getImage(id);
    }
}
