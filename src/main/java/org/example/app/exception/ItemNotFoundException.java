package org.example.app.exception;

public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(long itemId) {
        super("item by id " + itemId + " not found");
    }
}
