package org.example.app.exception;

public class LoginAlreadyRegisteredException extends RuntimeException {
    public LoginAlreadyRegisteredException(String userName) {
        super("user " + userName + " already registered");
    }

}
