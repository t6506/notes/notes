package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.UserEntity;
import org.example.app.exception.LoginAlreadyRegisteredException;
import org.example.framework.di.annotation.Component;
import org.example.framework.jpatemplate.JpaTransactionTemplate;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class UserRepository {
    private final JpaTransactionTemplate template;

    public UserEntity save(UserEntity entity) {
        return template.executeInTransaction(em -> {

            final String login = entity.getLogin();

            UserEntity exist = em.find(UserEntity.class, login);

            if (exist != null) {
                throw new LoginAlreadyRegisteredException(login);
            }

            em.persist(entity);
            return entity;
        });
    }

    public Optional<UserEntity> findByLogin(String login) {
        return template.executeInTransaction(em -> {
            UserEntity user = em.find(UserEntity.class, login);
            return Optional.ofNullable(user);
        });
    }

}
