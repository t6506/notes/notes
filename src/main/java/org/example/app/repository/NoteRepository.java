package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.NoteEntity;
import org.example.app.entity.UserEntity;
import org.example.app.exception.ItemNotFoundException;
import org.example.framework.di.annotation.Component;
import org.example.framework.jpatemplate.JpaTransactionTemplate;
import org.example.framework.security.auth.exception.UserNotAuthorizedException;

import javax.persistence.Query;
import java.util.List;

@RequiredArgsConstructor
@Component
public class NoteRepository {
    private final JpaTransactionTemplate template;

    public NoteEntity create(final NoteEntity entity) {
        return template.executeInTransaction(em -> {
            em.persist(entity);
            return entity;
        });
    }

    public NoteEntity updateContent(final long id, final String content, final String login) {
        return template.executeInTransaction(em -> {
            final NoteEntity updated = em.getReference(NoteEntity.class, id);
            checkAvailable(id, updated, login);
            updated.setContent(content);
            return updated;
        });
    }


    public NoteEntity saveImage(final long id, final byte[] image, final String login) {
        return template.executeInTransaction(em -> {
            final NoteEntity note = em.getReference(NoteEntity.class, id);
            checkAvailable(id, note, login);
            note.setImage(image);
            return note;
        });
    }

    public NoteEntity findById(final Long id, final String login) {
        return template.executeInTransaction(em -> {
            final NoteEntity note = em.find(NoteEntity.class, id);
            checkAvailable(id, note, login);
            return note;
        });
    }

    public void removeById(final long id, final String login) {
        template.executeInTransaction(em -> {
            final NoteEntity note = em.find(NoteEntity.class, id);
            checkAvailable(id, note, login);
            note.setRemoved(true);
            return note;
        });
    }

    public List<NoteEntity> findByOwner(UserEntity owner) {
        return template.executeInTransaction(em -> {
            final Query query = em.createQuery(
                    "SELECT n from NoteEntity n WHERE n.owner = :owner AND n.removed = false",
                    NoteEntity.class);
            return (List<NoteEntity>) query.setParameter("owner", owner).getResultList();
        });
    }

    public byte[] getImage(final long id, final String login) {
        return template.executeInTransaction(em -> {
            final NoteEntity note = em.find(NoteEntity.class, id);
            checkAvailable(id, note, login);
            return note.getImage();
        });
    }

    private void checkAvailable(final long id, final NoteEntity note, final String login) {
        if (note == null) {
            throw new ItemNotFoundException(id);
        }
        if (!note.getOwner().getLogin().equals(login)) {
            throw new UserNotAuthorizedException(login);
        }
        if (note.isRemoved()) {
            throw new ItemNotFoundException(id);
        }
    }


}
