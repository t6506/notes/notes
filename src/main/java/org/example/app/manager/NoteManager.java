package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


import org.example.app.dto.LoadImageRS;
import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;
import org.example.app.entity.NoteEntity;
import org.example.app.entity.UserEntity;
import org.example.app.repository.NoteRepository;
import org.example.app.repository.UserRepository;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.auth.exception.UserNotAuthenticatedException;
import org.example.framework.security.auth.principal.AnonymousPrincipal;
import org.example.framework.server.auth.SecurityContext;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class NoteManager {
    private final UserRepository userRepository;
    private final NoteRepository noteRepository;

    public NoteRS create(final NoteRQ createRQ) {
        final String login = getLogin();
        assertAuthenticated(login);
        final UserEntity owner = userRepository.findByLogin(login).get();

        final NoteEntity note = new NoteEntity();
        note.setOwner(owner);
        note.setContent(createRQ.getContent());
        final NoteEntity saved = noteRepository.create(note);
        return new NoteRS(saved.getContent());
    }

    public NoteRS getById(final long id) {
        final String login = getLogin();
        assertAuthenticated(login);
        final NoteEntity note = noteRepository.findById(id, login);
        if (note.getImage() != null) {
            return new NoteRS(note.getContent(), getImageLink(id));
        }
        return new NoteRS(note.getContent());
    }


    public NoteRS updateContent(final long id, final NoteRQ updateRQ) {
        final String login = getLogin();
        assertAuthenticated(login);
        final NoteEntity updated = noteRepository.updateContent(id, updateRQ.getContent(), login);
        return new NoteRS(updated.getContent());
    }

    public void removeById(final long id) {
        final String login = getLogin();
        assertAuthenticated(login);
        noteRepository.removeById(id, login);
    }

    public List<NoteRS> getAll() {
        final String login = getLogin();
        assertAuthenticated(login);
        final UserEntity owner = userRepository.findByLogin(login).get();
        final List<NoteEntity> noteList = noteRepository.findByOwner(owner);
        final ArrayList<NoteRS> result = new ArrayList<>(noteList.size());
        for (NoteEntity note : noteList) {
            if (note.getImage() != null) {
                result.add(new NoteRS(note.getContent(), getImageLink(note.getId())));
                continue;
            }
            result.add(new NoteRS(note.getContent()));
        }
        return result;
    }

    public LoadImageRS loadImage(final long id, final byte[] image) {
        final String login = getLogin();
        assertAuthenticated(login);
        final NoteEntity note = noteRepository.saveImage(id, image, login);
        return new LoadImageRS(note.getId(), getImageLink(id));
    }

    public byte[] getImage(final long id) {
        final String login = getLogin();
        assertAuthenticated(login);
        return noteRepository.getImage(id, login);
    }

    private void assertAuthenticated(final String login) {
        if (login.equals(AnonymousPrincipal.ANONYMOUS)) {
            throw new UserNotAuthenticatedException(login);
        }
    }
    private String getLogin() {
        return SecurityContext.getPrincipal().getName();
    }

    private String getImageLink(final long id) {
        return "https://localhost:8443/notes/" + id + "/image";
    }



}
