package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.entity.UserEntity;

import org.example.framework.di.annotation.Component;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.server.exception.UnsupportAuthenticationToken;
import org.example.app.repository.UserRepository;

import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;


@Slf4j
@Component
@RequiredArgsConstructor
public class UserManager implements Authenticator {

    private final UserRepository repo;
    private final PasswordEncoder encoder;

    public UserRegisterRS create(final UserRegisterRQ createRQ) {
        final String login = createRQ.getLogin().trim().toLowerCase();
        final String encodePassword = encoder.encode(createRQ.getPassword());
        UserEntity user = new UserEntity(login, encodePassword);
        UserEntity saved = repo.save(user);
        log.debug("create user: {}", saved);
        return new UserRegisterRS(login);
    }

    @Override
    public boolean authenticate(final AuthenticationToken request) throws UnsupportAuthenticationToken {
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportAuthenticationToken();
        }

        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();
        final String encodedPassword;

        if (login == null) {
            return false;
        }

        Optional<UserEntity> user = repo.findByLogin(login);
        if (!user.isPresent()) {
            return false;
        }
        encodedPassword = user.get().getEncodePassword();
        return encoder.matches(password, encodedPassword);
    }
}
