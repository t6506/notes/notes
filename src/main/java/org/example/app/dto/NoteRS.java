package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class NoteRS {
    private final String content;
    private final String imageLink;

    public NoteRS(final String content) {
        this.content = content;
        this.imageLink = "";
    }
}
