package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class LoadImageRS {
    private final long id;
    private final String imageLink;
}
