package org.example.app.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ConfigureDb {
    private final static String DB_URL = "DB_URL";
    private final static String DEFAULT_URL = "jdbc:postgresql://localhost:5430/db";

    public static Map<String, Object> setUrl() {
        final String dbUrl = Optional.ofNullable(System.getenv(DB_URL))
                .orElse(DEFAULT_URL);
        final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.url", dbUrl);
        return properties;
    }
}
